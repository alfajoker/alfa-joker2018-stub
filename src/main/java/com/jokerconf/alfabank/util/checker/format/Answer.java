package com.jokerconf.alfabank.util.checker.format;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = SingleAnswer.class, name = "single"),
        @JsonSubTypes.Type(value = ListOfPairAnswer.class, name = "listOfPair"),
        @JsonSubTypes.Type(value = PairAnswer.class, name = "pairAnswer")
})
public abstract  class Answer {
}
