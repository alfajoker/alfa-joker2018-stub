package com.jokerconf.alfabank.util.checker.format;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

@Data
@EqualsAndHashCode
@AllArgsConstructor
public class ListOfPairAnswer extends Answer {
    private Map<Object, Object> listOfPairs;

    public static ListOfPairAnswer listOfPairs() {
        return new ListOfPairAnswer(new HashMap<>());
    }

    public ListOfPairAnswer addPair(Object key, Object value) {
        listOfPairs.put(key, value);
        return this;
    }

}
