package com.jokerconf.alfabank.util.checker.format;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@AllArgsConstructor
public class PairAnswer extends Answer {
    private long key;
    private long value;

    public static PairAnswer pairResult(long key, long value) {
        return new PairAnswer(key, value);
    }
}
