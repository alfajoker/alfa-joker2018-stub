package com.jokerconf.alfabank.task;

import com.jokerconf.alfabank.domain.Payment;
import org.apache.kafka.clients.consumer.Consumer;
import org.junit.Test;

import java.time.LocalDateTime;

import static com.jokerconf.alfabank.config.GameConfig.EXAMPLE_TOPIC;
import static com.jokerconf.alfabank.config.KafkaConfig.getDefaultProperties;
import static com.jokerconf.alfabank.util.checker.DemoChecker.checkDemoAnswer;
import static com.jokerconf.alfabank.util.checker.format.ListOfPairAnswer.listOfPairs;
import static com.jokerconf.alfabank.util.kafka.KafkaConsumerFactory.createPaymentConsumer;
import static com.jokerconf.alfabank.util.kafka.KafkaOffsetUtils.isTopicConsumed;
import static com.jokerconf.alfabank.util.kafka.KafkaOffsetUtils.subscribeAndSeekToBeginning;

public class AccountLinks {
    private static final int TASK_NUMBER_4 = 4;

    @Test
    public void task4Solution() {
        LocalDateTime from = LocalDateTime.parse("2018-09-19T15:10:32");
        LocalDateTime to = LocalDateTime.parse("2018-10-16T15:10:02");

        try (Consumer<String, Payment> paymentConsumer = createPaymentConsumer(getDefaultProperties())) {
            // Use EXAMPLE_TOPIC to verify your solution, PAYMENTS_TOPIC to solve it.
            subscribeAndSeekToBeginning(paymentConsumer, EXAMPLE_TOPIC);

            do {

                // Solution code
                // ...

            } while (!isTopicConsumed(paymentConsumer, EXAMPLE_TOPIC));

            // Checker
            // sendAnswer(TASK_NUMBER_4, listOfPairs()
            //                    .addPair(?, ?)
            //                    ...
            // );
            checkDemoAnswer(TASK_NUMBER_4, listOfPairs()
                    .addPair("ZK4451", 2)
                    .addPair("ZK4364", 2)
                    .addPair("ZK4361", 2)
                    .addPair("ZK4460", 2)
                    .addPair("ZK4418", 2)
                    .addPair("ZK4409", 2)
                    .addPair("ZK4415", 1)
                    .addPair("ZK4448", 2)
                    .addPair("ZK4367", 1)
                    .addPair("ZK4421", 2)
                    .addPair("ZK4355", 1)
                    .addPair("ZK4454", 2)
                    .addPair("ZK4358", 1)
                    .addPair("ZK4457", 2)
                    .addPair("ZK4412", 2)
            );
        }
    }
}
