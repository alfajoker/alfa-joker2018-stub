package com.jokerconf.alfabank.util.checker.format;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@AllArgsConstructor
public class SingleAnswer extends Answer {
    private long value;

    public static SingleAnswer singleResult(long answer) {
        return new SingleAnswer(answer);
    }
}
