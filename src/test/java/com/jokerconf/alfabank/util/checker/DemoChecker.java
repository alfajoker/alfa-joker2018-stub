package com.jokerconf.alfabank.util.checker;

import com.jokerconf.alfabank.util.checker.format.Answer;

import static com.jokerconf.alfabank.util.checker.format.ListOfPairAnswer.listOfPairs;
import static com.jokerconf.alfabank.util.checker.format.PairAnswer.pairResult;
import static com.jokerconf.alfabank.util.checker.format.SingleAnswer.singleResult;
import static org.junit.Assert.assertEquals;

public class DemoChecker {
    public static void checkDemoAnswer(int taskNumber, Answer result) {
        switch (taskNumber) {
            case 1: {
                assertEquals(result, singleResult(592));
                break;
            }
            case 2: {
                assertEquals(result, listOfPairs().addPair(0, 14).addPair(1, 16).addPair(2, 3));
                break;
            }
            case 3: {
                assertEquals(result, pairResult(296, 296));
                break;
            }
            case 4: {
                assertEquals(result, listOfPairs()
                        .addPair("ZK4451", 2L)
                        .addPair("ZK4364", 2L)
                        .addPair("ZK4361", 2L)
                        .addPair("ZK4460", 2L)
                        .addPair("ZK4418", 2L)
                        .addPair("ZK4409", 2L)
                        .addPair("ZK4415", 1L)
                        .addPair("ZK4448", 2L)
                        .addPair("ZK4367", 1L)
                        .addPair("ZK4421", 2L)
                        .addPair("ZK4355", 1L)
                        .addPair("ZK4454", 2L)
                        .addPair("ZK4358", 1L)
                        .addPair("ZK4457", 2L)
                        .addPair("ZK4412", 2L));
                break;
            }
            case 5: {
                assertEquals(result, singleResult(3));
                break;
            }
        }
    }
}