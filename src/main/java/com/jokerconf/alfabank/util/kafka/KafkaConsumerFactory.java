package com.jokerconf.alfabank.util.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jokerconf.alfabank.domain.Payment;
import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Map;
import java.util.Properties;

public class KafkaConsumerFactory {

    public static Consumer<String, Payment> createPaymentConsumer(Properties properties) {
        return new KafkaConsumer<>(properties, new StringDeserializer(), new PaymentDeserializer());
    }

    private static class PaymentDeserializer implements Deserializer<Payment> {
        private final ObjectMapper mapper = new ObjectMapper();

        @Override
        public void configure(Map<String, ?> configs, boolean isKey) {
            // nothing to do
        }

        @Override
        @SneakyThrows
        public Payment deserialize(String topic, byte[] data) {
            if (data == null)
                return null;
            else {
                return mapper.readValue(data, Payment.class);
            }
        }

        @Override
        public void close() {
            // nothing to do
        }
    }
}
