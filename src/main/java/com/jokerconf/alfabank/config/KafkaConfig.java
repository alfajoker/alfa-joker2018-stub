package com.jokerconf.alfabank.config;

import java.util.Properties;

import static com.jokerconf.alfabank.config.GameConfig.PASSWORD;
import static com.jokerconf.alfabank.config.GameConfig.USERNAME;

public class KafkaConfig {
    public static Properties getDefaultProperties() {
        return defaultConfiguration(USERNAME, PASSWORD);
    }

    public static Properties getSinglePullProperties() {
        Properties properties = defaultConfiguration(USERNAME, PASSWORD);
        properties.put("max.poll.records", 1);

        return properties;
    }

    private static Properties defaultConfiguration(String username, String password) {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "kafka1.alfaconf.pro:9092,kafka2.alfaconf.pro:9092,kafka3.alfaconf.pro:9092");

        properties.put("security.protocol", "SASL_SSL");
        properties.put("sasl.mechanism", "PLAIN");
        properties.put("ssl.endpoint.identification.algorithm", "");
        properties.put("ssl.truststore.location", "./src/main/resources/CA.jks");
        properties.put("ssl.truststore.password", "123456");

        String jaasTemplate = "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"%s\" password=\"%s\";";
        String jaasCfg = String.format(jaasTemplate, username, password);
        properties.put("sasl.jaas.config", jaasCfg);

        properties.put("group.id", username);
        properties.put("client.id", username);
        properties.put("auto.offset.reset", "earliest");
        properties.put("auto.commit.interval.ms", 100);
        properties.put("enable.auto.commit", true);

        return properties;
    }
}
