package com.jokerconf.alfabank.task;

import com.jokerconf.alfabank.domain.Payment;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.clients.consumer.Consumer;
import org.junit.Test;

import java.util.List;

import static com.jokerconf.alfabank.config.KafkaConfig.getSinglePullProperties;
import static com.jokerconf.alfabank.util.checker.DemoChecker.checkDemoAnswer;
import static com.jokerconf.alfabank.util.checker.format.ListOfPairAnswer.listOfPairs;
import static com.jokerconf.alfabank.util.kafka.KafkaConsumerFactory.createPaymentConsumer;
import static java.util.Arrays.asList;

public class OffsetJumper {
    private static final int TASK_NUMBER_2 = 2;

    @Test
    public void task2Solution() {
        List<Pair<Integer, Integer>> conditions = asList(
                Pair.of(113, 17),
                Pair.of(114, 14),
                Pair.of(117, 17)
        );

        try (Consumer<String, Payment> paymentConsumer = createPaymentConsumer(getSinglePullProperties())) {
            // Use EXAMPLE_TOPIC to verify your solution, PAYMENTS_TOPIC to solve it.

            // Solution code
            // ...
            // Use: paymentConsumer.assign(); paymentConsumer.seek(); paymentConsumer.resume();

        }

        // Checker
        //sendAnswer(TASK_NUMBER_2, listOfPairs() -- for your solution from PAYMENTS_TOPIC
        //        .addPair(?, ?)
        //        .addPair(?, ?)
        //        .addPair(?, ?)
        //);

        checkDemoAnswer(TASK_NUMBER_2, listOfPairs()
                .addPair(0, 14)
                .addPair(1, 16)
                .addPair(2, 3)
        );
    }
}
