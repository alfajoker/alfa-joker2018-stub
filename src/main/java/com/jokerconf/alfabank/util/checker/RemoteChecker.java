package com.jokerconf.alfabank.util.checker;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jokerconf.alfabank.util.checker.format.Answer;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.nio.charset.Charset;

import static com.jokerconf.alfabank.config.GameConfig.TOKEN;
import static java.nio.charset.Charset.defaultCharset;
import static org.apache.commons.io.IOUtils.readLines;

@Slf4j
public class RemoteChecker {
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final String checkerUrl = "http://kafka.alfaconf.pro:8080/tasks/%s/check";

    @SneakyThrows
    public static void sendAnswer(int taskNumber, Answer result) {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpPost post = new HttpPost(String.format(checkerUrl, taskNumber));
            String sourceAnswer = mapper.writeValueAsString(result);
            log.info("For task: {} your answer: {}", taskNumber, sourceAnswer);
            StringEntity postingString = new StringEntity(mapper.writeValueAsString(result));
            post.setEntity(postingString);
            post.setHeader("Content-type", "application/json");
            post.setHeader("token", TOKEN);

            CloseableHttpResponse response = client.execute(post);

            StatusLine status = response.getStatusLine();
            if (status.getStatusCode() != 200) {
                throw new RuntimeException("Http code: "
                        + status.getStatusCode()
                        + " reason: "
                        + readLines(response.getEntity().getContent(), defaultCharset()));
            }
        }
    }
}
