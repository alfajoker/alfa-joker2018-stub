package com.jokerconf.alfabank.task;

import com.jokerconf.alfabank.domain.Payment;
import org.apache.kafka.clients.consumer.Consumer;
import org.junit.Test;

import java.time.LocalDateTime;

import static com.jokerconf.alfabank.config.GameConfig.EXAMPLE_TOPIC;
import static com.jokerconf.alfabank.config.KafkaConfig.getDefaultProperties;
import static com.jokerconf.alfabank.util.checker.DemoChecker.checkDemoAnswer;
import static com.jokerconf.alfabank.util.checker.format.PairAnswer.pairResult;
import static com.jokerconf.alfabank.util.kafka.KafkaConsumerFactory.createPaymentConsumer;
import static com.jokerconf.alfabank.util.kafka.KafkaOffsetUtils.isTopicConsumed;
import static com.jokerconf.alfabank.util.kafka.KafkaOffsetUtils.subscribeAndSeekToBeginning;

public class SimpleSum {
    private static final int TASK_NUMBER_3 = 3;

    @Test
    public void task3Solution() {
        LocalDateTime from = LocalDateTime.parse("2018-09-19T15:10:32");
        LocalDateTime to = LocalDateTime.parse("2018-10-16T15:10:02");

        try (Consumer<String, Payment> paymentConsumer = createPaymentConsumer(getDefaultProperties())) {
            // Use EXAMPLE_TOPIC to verify your solution, PAYMENTS_TOPIC to solve it.
            subscribeAndSeekToBeginning(paymentConsumer, EXAMPLE_TOPIC);

            do {

                // Solution code
                // ...

            } while (!isTopicConsumed(paymentConsumer, EXAMPLE_TOPIC));

            // Checker
            //sendAnswer(TASK_NUMBER_3, pairResult(?, ?)); -- for your solution from PAYMENTS_TOPIC
            checkDemoAnswer(TASK_NUMBER_3, pairResult(296, 296));
        }
    }
}
