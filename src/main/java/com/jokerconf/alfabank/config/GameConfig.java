package com.jokerconf.alfabank.config;

public class GameConfig {
    // Your credentials
    public static final String TOKEN = "";
    public static final String USERNAME = "";
    public static final String PASSWORD = "";

    // Don't change it!
    public static final String PAYMENTS_TOPIC = "payments";
    public static final String EXAMPLE_TOPIC = "junior_payments";
}
