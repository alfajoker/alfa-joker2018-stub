package com.jokerconf.alfabank.util.kafka;

import com.jokerconf.alfabank.domain.Payment;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class KafkaOffsetUtils {
    public static void subscribeAndSeekToBeginning(Consumer<String, Payment> consumer, String topic) {
        List<TopicPartition> partitions = asList(new TopicPartition(topic, 0), new TopicPartition(topic, 1), new TopicPartition(topic, 2));
        consumer.assign(partitions);
        partitions.forEach(partition -> consumer.seek(partition, 0));
    }

    public static boolean isTopicConsumed(Consumer<String, Payment> consumer, String topicId) {
        return getConsumerGroupLag(consumer, topicId) == 0;
    }

    public static long getConsumerGroupLag(Consumer<String, Payment> consumer, String topicId) {
        List<TopicPartition> topicPartitions = topicPartitionsFor(consumer, topicId);

        Map<TopicPartition, Long> endOffsets = consumer.endOffsets(topicPartitions);
        Map<TopicPartition, Long> currentPositions = committedPositions(consumer, topicPartitions);

        return endOffsets.entrySet().stream()
                .mapToLong(endOffset -> endOffset.getValue() - currentPositions.get(endOffset.getKey()))
                .sum();

    }

    private static Map<TopicPartition, Long> committedPositions(Consumer<String, Payment> consumer, List<TopicPartition> topicPartitions) {
        return topicPartitions.stream()
                .collect(Collectors.toMap(partitionInfo -> partitionInfo, partitionInfo -> {
                    OffsetAndMetadata metadata = consumer.committed(partitionInfo);
                    return metadata != null ? metadata.offset() : 0;
                }));
    }

    private static List<TopicPartition> topicPartitionsFor(Consumer<String, Payment> consumer, String topicId) {
        return consumer.partitionsFor(topicId).stream()
                .map(partitionInfo -> new TopicPartition(partitionInfo.topic(), partitionInfo.partition()))
                .collect(toList());
    }

}
