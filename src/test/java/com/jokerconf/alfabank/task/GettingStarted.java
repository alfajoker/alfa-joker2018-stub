package com.jokerconf.alfabank.task;

import com.jokerconf.alfabank.domain.Payment;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.jokerconf.alfabank.config.GameConfig.EXAMPLE_TOPIC;
import static com.jokerconf.alfabank.config.KafkaConfig.getDefaultProperties;
import static com.jokerconf.alfabank.util.checker.DemoChecker.checkDemoAnswer;
import static com.jokerconf.alfabank.util.checker.format.SingleAnswer.singleResult;
import static com.jokerconf.alfabank.util.kafka.KafkaConsumerFactory.createPaymentConsumer;
import static com.jokerconf.alfabank.util.kafka.KafkaOffsetUtils.isTopicConsumed;
import static com.jokerconf.alfabank.util.kafka.KafkaOffsetUtils.subscribeAndSeekToBeginning;
import static java.time.Duration.ofSeconds;

public class GettingStarted {
    private static final int TASK_NUMBER_1 = 1;

    @Test
    public void task1Solution() {
        try (Consumer<String, Payment> paymentConsumer = createPaymentConsumer(getDefaultProperties())) {
            // Use EXAMPLE_TOPIC to verify your solution, PAYMENTS_TOPIC to solve it.
            subscribeAndSeekToBeginning(paymentConsumer, EXAMPLE_TOPIC);

            List<Payment> payments = new ArrayList<>();
            do {
                ConsumerRecords<String, Payment> records = paymentConsumer.poll(ofSeconds(1));
                records.forEach(record -> payments.add(record.value()));
            } while (!isTopicConsumed(paymentConsumer, EXAMPLE_TOPIC));

            // Solution code
            // ...

            // Checker
            //sendAnswer(TASK_NUMBER_1, singleResult(?)); -- for your solution from PAYMENTS_TOPIC
            checkDemoAnswer(TASK_NUMBER_1, singleResult(592));
        }
    }
}
